FuRegisterClass("MatchTint", CT_Tool, {
	REGS_Category = "Color",
	REGS_Name = "Match Tint",
	REGS_OpIconString = "MTnT",
	REGS_OpDescription = "A Tool that Matches the Tint Of Another Image.",
	REG_Version = 130,
	REGS_Company = "Learn Now FX",
	REGS_URL = "http://LearnNowFX.com",
	REG_Fuse_NoEdit	= true,
    REG_Fuse_NoReload = true,
})

TintParams = [[ 
  float colorW[3];
  float colorB[3];
  float amount;
  float saturation;
  int prePost;
  int srcCompOrder;
]]

TintKernel = [[
__KERNEL__ void TintKernel(__CONSTANTREF__ TintParams *params, __TEXTURE2D__ src,  __TEXTURE2D_WRITE__ dst) {
	DEFINE_KERNEL_ITERATORS_XY(x, y);
	float4 image = _tex2DVecN(src, x, y, params->srcCompOrder);
	float3 white = to_float3_v(params->colorW);
	float3 black = to_float3_v(params->colorB);
	float amount = params->amount/100.0f;

if(params->prePost == 1){
  if(image.w != 0.0f){	
	image.x /= image.w;
	image.y /= image.w;
	image.z /= image.w;
  }
}		
      float saturation = params->saturation;
      float luma = (image.x * 0.299f) + (image.y * 0.587f) + (image.z * 0.114f);
	  image.x = saturation * image.x + (1.0f - saturation) * luma;
	  image.y = saturation * image.y + (1.0f - saturation) * luma;
	  image.z = saturation * image.z + (1.0f - saturation) * luma;
		
	  float3 color = to_float3(image.x, image.y, image.z);
	  float3 map = color;
	  map.x = _mix(black.x, white.x, color.x);
	  map.y = _mix(black.y, white.y, color.y);
	  map.z = _mix(black.z, white.z, color.z);


	  color.x = _mix(color.x, map.x, amount);
	  color.y = _mix(color.y, map.y, amount);
	  color.z = _mix(color.z, map.z, amount);


		
	  image.x = color.x;
	  image.y = color.y;
	  image.z = color.z;

if(params->prePost == 1){
	image.x *= image.w;
	image.y *= image.w;
	image.z *= image.w;
}		  
		
		_tex2DVec4Write(dst, x, y, image);
	}
]]

SwapColors = [[
if not tool then
	tool = comp.ActiveTool
end
function Swap()
   local r1 = tool:GetInput("ShadowsRed", fu.TIME_UNDEFINED)
   local g1 = tool:GetInput("ShadowsGreen", fu.TIME_UNDEFINED)
   local b1 = tool:GetInput("ShadowsBlue", fu.TIME_UNDEFINED)
   local r2 = tool:GetInput("HighLightsRed", fu.TIME_UNDEFINED)
   local g2 = tool:GetInput("HighLightsGreen", fu.TIME_UNDEFINED)
   local b2 = tool:GetInput("HighLightsBlue", fu.TIME_UNDEFINED)
   tool:SetInput('ShadowsRed', r2, fu.TIME_UNDEFINED)
   tool:SetInput('ShadowsGreen', g2, fu.TIME_UNDEFINED)
   tool:SetInput('ShadowsBlue', b2, fu.TIME_UNDEFINED)
   tool:SetInput('HighLightsRed', r1, fu.TIME_UNDEFINED)
   tool:SetInput('HighLightsGreen',g1, fu.TIME_UNDEFINED)
   tool:SetInput('HighLightsBlue', b1, fu.TIME_UNDEFINED)
end   
Swap()	
]]


function Create()
InLabel = self:AddInput("Match Tint | By Learn Now FX", "version", {
	LINKID_DataType			 = "Text",
	INPID_InputControl	 = "LabelControl",
	INP_External				 = false,
    INP_Passive					 = true,
	IC_ControlPage = -1,
})
 self:BeginControlNest("Pre-Tint Saturation", "PreTintSaturation", true, {})	
InSaturation = self:AddInput("Saturation", "Saturation", {
	LINKID_DataType     = "Number",
	INPID_InputControl  = "SliderControl",
	INP_MinScale        = 0.0,
	INP_MaxScale        = 2.0,
	INP_Default         = 1.0,
})
self:EndControlNest()
InSwapColors = self:AddInput("Swap Colors", "SwapColors", {
	LINKID_DataType = "Number",
	INPID_InputControl = "ButtonControl",
	INP_DoNotifyChanged = true,
	BTNCS_Execute = SwapColors,
	ICD_Width = 1.0,
})
self:BeginControlNest("Map Black To", "MapBlackTo", true, {})
InColorR2 = self:AddInput("Shadows Red", "ShadowsRed", {
    ICS_Name            = "Map Black To",
    LINKID_DataType     = "Number",
    INPID_InputControl  = "ColorControl",
    INP_Default         = 0.0,
    INP_MaxScale        = 1.0,
    CLRC_ShowWheel      = true,
    IC_ControlGroup     = 2,
    IC_ControlID        = 0,
})
InColorG2 = self:AddInput("Shadows Green", "ShadowsGreen", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ColorControl",
    INP_Default  = 0.0,
    IC_ControlGroup  = 2,
    IC_ControlID = 1,
})
InColorB2 = self:AddInput("Shadows Blue", "ShadowsBlue", {
    LINKID_DataType     = "Number",
    INPID_InputControl  = "ColorControl",
    INP_Default         = 0.0,
    IC_ControlGroup     = 2,
    IC_ControlID        = 2,	
})
 self:EndControlNest()
 self:BeginControlNest("Map White To", "MapWhiteTo", true, {})
InColorR1 = self:AddInput("HighLights Red", "HighLightsRed", {
    ICS_Name            = "Map White To",
    LINKID_DataType     = "Number",
    INPID_InputControl  = "ColorControl",
    INP_Default         = 1.0,
    INP_MaxScale        = 1.0,
    CLRC_ShowWheel      = true,
    IC_ControlGroup     = 1,
    IC_ControlID        = 0,
})
InColorG1 = self:AddInput("HighLights Green", "HighLightsGreen", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ColorControl",
    INP_Default  = 1.0,
    IC_ControlGroup  = 1,
    IC_ControlID = 1,
})
InColorB1 = self:AddInput("HighLights Blue", "HighLightsBlue", {
    LINKID_DataType     = "Number",
    INPID_InputControl  = "ColorControl",
    INP_Default         = 1.0,
    IC_ControlGroup     = 1,
    IC_ControlID        = 2,	
})
 self:EndControlNest()	 
 self:BeginControlNest("Controls", "Controls", true, {})
InAmount = self:AddInput("Amount To Tint", "AmountToTint", {
	LINKID_DataType     = "Number",
	INPID_InputControl  = "SliderControl",
	INP_MinScale        = 0.0,
	INP_MaxScale        = 100.0,
	INP_Default         = 100.0,
})
InPreDividePostMultiply = self:AddInput("Pre-Divide/Post-Multiply", "PreDividePostMultiply", {
	LINKID_DataType     = "Number",
	INPID_InputControl  = "CheckboxControl",
	INP_Integer         = true,
	ICD_Width           = 1,
	INP_Default         = 0,
})
 self:EndControlNest()
InImage = self:AddInput("Image", "Image", {
	LINKID_DataType = "Image",
	LINK_Main = 1,
})
OutImage = self:AddOutput("Output", "Output", {
	LINKID_DataType = "Image",
	LINK_Main = 1,
})
end

local lastreqtime = -2

function Process(req)
	local src = InImage:GetValue(req)
	local dst = Image{ IMG_Like = src, IMG_DeferAlloc = true }
	local node = DVIPComputeNode(req, "TintKernel", TintKernel, "TintParams", TintParams)


if (lastreqtime ~= req.Time - 1) then
    params = node:GetParamBlock(TintParams)
end
lastreqtime = req.Time


if (InColorR1:GetValue(req).Value ~= nil) then params.colorW[0] = InColorR1:GetValue(req).Value end
if (InColorG1:GetValue(req).Value ~= nil) then params.colorW[1] = InColorG1:GetValue(req).Value end
if (InColorB1:GetValue(req).Value ~= nil) then params.colorW[2] = InColorB1:GetValue(req).Value end
if (InColorR2:GetValue(req).Value ~= nil) then params.colorB[0] = InColorR2:GetValue(req).Value end
if (InColorG2:GetValue(req).Value ~= nil) then params.colorB[1] = InColorG2:GetValue(req).Value end
if (InColorB2:GetValue(req).Value ~= nil) then params.colorB[2] = InColorB2:GetValue(req).Value end
if (InAmount:GetValue(req).Value ~= nil) then params.amount = InAmount:GetValue(req).Value end	
if (InSaturation:GetValue(req).Value ~= nil) then params.saturation = InSaturation:GetValue(req).Value end
if (InPreDividePostMultiply:GetValue(req).Value ~= nil) then params.prePost = InPreDividePostMultiply:GetValue(req).Value end		
	params.srcCompOrder = src:IsMask() and 1 or 15

	node:SetParamBlock(params)
	node:AddInput("src", src)
	node:AddOutput("dst", dst)

	local ok = node:RunSession(req)

	if not ok then
	  dst = nil
	  dump(node:GetErrorLog())
	end

	OutImage:Set(req, dst)
end
