--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Sept, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_Lightwrap = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				CommentsNest = Input { Value = 0, },
				Comments = Input { Value = "Macro by Emilio Sapia - Millolab\nhttps://emiliosapia.myportfolio.com", },
				MainInput1 = InstanceInput {
					SourceOp = "PipeRouter1",
					Source = "Input",
				},
				Filter = InstanceInput {
					SourceOp = "Bitmap1",
					Source = "Filter",
				},
				Spread = InstanceInput {
					SourceOp = "Blur1",
					Source = "XBlurSize",
					Name = "Spread",
					MaxScale = 5,
					Default = 2,
				},
				Gain = InstanceInput {
					SourceOp = "BrightnessContrast2",
					Source = "Gain",
					Default = 5,
				},
				Falloff = InstanceInput {
					SourceOp = "BrightnessContrast2",
					Source = "Gamma",
					Name = "Falloff",
					Default = 1,
				},
				ClippingMode = InstanceInput {
					SourceOp = "Blur1",
					Source = "ClippingMode",
				},
				PaintMode = InstanceInput {
					SourceOp = "Bitmap1",
					Source = "PaintMode",
				},
				Invert = InstanceInput {
					SourceOp = "Bitmap2",
					Source = "Invert",
					Default = 0,
				},
				EffectMask = InstanceInput {
					SourceOp = "Bitmap1",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Bitmap1",
					Source = "Mask",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 1150.9, 290.707 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ConnectedSnap = true,
					AutoSnap = true,
					AspectPics = false,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 696.179, 811.966, 189.96, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 132.426, -2.21529 }
			},
			Tools = ordered() {
				Normalize = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						MultiplyByMask = Input { Value = 1, },
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0.1, },
						Input = Input {
							SourceOp = "Bitmap2",
							Source = "Mask",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -145.42, 159.701 } },
				},
				PipeRouter1 = PipeRouter {
					ViewInfo = PipeRouterInfo { Pos = { 32.4971, 37.3132 } },
				},
				Blur1 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						XBlurSize = Input { Value = 2, },
						Input = Input {
							SourceOp = "Normalize",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 223.08 } },
				},
				ChannelBooleans2_1 = ChannelBoolean {
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "min(Blur1.XBlurSize,1)", },
						Operation = Input { Value = 1, },
						Background = Input {
							SourceOp = "Blur1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur1_2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 284.032 } },
				},
				Blur1_2 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "Blur1.Filter",
						},
						XBlurSize = Input {
							Value = 6,
							Expression = "Blur1.XBlurSize*3",
						},
						ClippingMode = Input { Expression = "Blur1.ClippingMode", },
						Input = Input {
							SourceOp = "Normalize",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -145.42, 284.032 } },
				},
				ChannelBooleans2_4 = ChannelBoolean {
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "min(Blur1.XBlurSize,1)", },
						Operation = Input { Value = 1, },
						Background = Input {
							SourceOp = "ChannelBooleans2_1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur1_5",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 357.062 } },
				},
				Blur1_5 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "Blur1.Filter",
						},
						XBlurSize = Input {
							Value = 16,
							Expression = "Blur1.XBlurSize*8",
						},
						ClippingMode = Input { Expression = "Blur1.ClippingMode", },
						Input = Input {
							SourceOp = "Normalize",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -145.42, 357.062 } },
				},
				ChannelBooleans2_3 = ChannelBoolean {
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "min(Blur1.XBlurSize,1)", },
						MultiplyByMask = Input { Value = 1, },
						Operation = Input { Value = 1, },
						Background = Input {
							SourceOp = "ChannelBooleans2_4",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur1_4",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 426.768 } },
				},
				Blur1_4 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "Blur1.Filter",
						},
						XBlurSize = Input {
							Value = 42,
							Expression = "Blur1.XBlurSize*21",
						},
						ClippingMode = Input { Expression = "Blur1.ClippingMode", },
						Input = Input {
							SourceOp = "Normalize",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -145.42, 426.768 } },
				},
				ChannelBooleans2 = ChannelBoolean {
					Inputs = {
						EffectMask = Input {
							SourceOp = "Bitmap2",
							Source = "Mask",
						},
						ApplyMaskInverted = Input { Value = 1, },
						MultiplyByMask = Input { Value = 1, },
						Background = Input {
							SourceOp = "ChannelBooleans2_3",
							Source = "Output",
						},
						CommentsNest = Input { Value = 0, },
						FrameRenderScriptNest = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 565.797 } },
				},
				BrightnessContrast2 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "min(Blur1.XBlurSize,1)", },
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 5, },
						PreDividePostMultiply = Input { Value = 1, },
						Input = Input {
							SourceOp = "ChannelBooleans2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 659.647 } },
				},
				Bitmap1 = BitmapMask {
					CtrlWShown = false,
					Inputs = {
						ShowViewControls = Input { Value = 0, },
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Image = Input {
							SourceOp = "BrightnessContrast2",
							Source = "Output",
						},
						Channel = Input { Value = FuID { "Luminance" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 6.58004, 755.112 } },
				},
				Bitmap2 = BitmapMask {
					Inputs = {
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Image = Input {
							SourceOp = "Invert",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 116.499, 129.18 } },
				},
				Invert = ChannelBoolean {
					NameSet = true,
					Inputs = {
						Operation = Input { Value = 10, },
						Background = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 116.499, 82.1317 } },
				}
			},
		}
	},
	ActiveTool = "ml_Lightwrap"
}