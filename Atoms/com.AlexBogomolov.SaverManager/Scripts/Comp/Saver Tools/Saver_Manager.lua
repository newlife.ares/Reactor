-- Description: 
--         Saver Manager. Solo selected saver(s), deselect all or enable all. 
--         Make loader from saver (requires dedicated -- script, installed from Reactor).
--         Set Saver In and Out
-- License: MIT
-- Author: Alex Bogomolov
-- email: mail@abogomolov.com
-- Donate: paypal.me/aabogomolov
-- Version: 1.2, 2020/9/1

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width,height = 250,150

app:AddConfig("SManager", {
    Target {
        ID = "SManager",
    },
    Hotkeys {
        Target = "SManager",
        Defaults = true,
        ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
    },
})

win = disp:AddWindow({
    ID = 'SManage',
    TargetID = 'SManager',
    WindowTitle = 'Saver Manager',
    Geometry = {100, 400, width, height},
    Spacing = 5,
    
    ui:VGroup{
        ID = 'root',
        ui:HGroup{VMargin = 10,
          ui:Button{ID = 'Solo', Text = 'Solo Selected',},
        },
        ui:HGroup{
          ui.Button{ID= 'SelectAll', Text = 'Select All'},
          ui:Button{ID = 'Enable', Text = 'Enable All',},
        },
        ui:VGroup{
          ui.Button{ID= 'CreateLoaders', Text = 'Make Loader'}
        },
        ui:HGroup{
            VMargin = 3,
            ui:Button{ ID = 'setIn', Text = 'saver IN', },
            ui:Button{ ID = 'setOut', Text = 'saver OUT', }
        },
     },
})

-- The window was closed
function win.On.SManage.Close(ev)
    disp:ExitLoop()
end

function check_selected(tool)
    return tool:GetAttrs('TOOLB_Selected')
end

function check_enabled(tool)
    return tool:GetAttrs('TOOLB_PassThrough')
end
    
function win.On.setIn.Clicked (ev)
    local tool = comp:GetToolList(true, 'Saver')[1]
    if tool then
        tool:SetAttrs({TOOLNT_EnabledRegion_Start = comp.CurrentTime})
    end
end

function win.On.setOut.Clicked (ev)
    local tool = comp:GetToolList(true, 'Saver')[1]
    if tool then
        tool:SetAttrs({TOOLNT_EnabledRegion_End = comp.CurrentTime})
    end
end

function win.On.Solo.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local selectedSavers = cmp:GetToolList(true, "Saver")
    local allSavers = cmp:GetToolList(false, "Saver")
    for _, currentSaver in pairs(allSavers) do
        if not check_selected(currentSaver) then
            currentSaver:SetAttrs( { TOOLB_PassThrough = true } )
        end
    end
    for _, sel in pairs(selectedSavers) do
        if check_enabled(sel) then
            sel:SetAttrs({ TOOLB_PassThrough = false})
        end
    end
end

function win.On.Enable.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local allSavers = cmp:GetToolList(false, "Saver")
        for i, currentSaver in pairs(allSavers) do
            currentSaver:SetAttrs( { TOOLB_PassThrough = false } )
        end
end

function win.On.SelectAll.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local allSavers = cmp:GetToolList(false, "Saver")
    flow = cmp.CurrentFrame.FlowView
    for _, sav in pairs(allSavers) do
        flow:Select(sav)
    end
end

function win.On.CreateLoaders.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    cmp:RunScript('Scripts:Comp/Saver Tools/LoaderFromSaver.lua')
end

win:Show()
disp:RunLoop()
win:Hide()
