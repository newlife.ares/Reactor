--[[

Suck Less Write On - Modifier Fuse

-------------------------------------------------------------------
Copyright (c) 2019-2020,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-------------------------------------------------------------------

too many late nights:

version 0.1	- 20191114 - initial test version, as Word Write On - https://www.steakunderwater.com/wesuckless/viewtopic.php?p=27080#p27080
version 0.2 - 20191115 - does not delete existing text from input when adding Modifier (thank you Peter Loveday)
version 0.3 - 20191116 - added lines and characters, renamed to Suck Less Write On
version 0.4 - 20191118 - introduce order of operations
version 0.5 - 20191119 - rewrite order of operations more cleverly (yay Lua!)
version 0.6 - 20191120 - implement negative searches
version 0.7 - 20191121 - extensive bug fixing (there were many)
version 0.8 - 20191122 - character count skips spaces now, more bug fixes (my brain hurts)
version 0.9 - 20191125 - relative mode, rewiring some logic
version 1.0 - 20191126 - argh aaargh aaaaaaaaargh arse arse arse
version 1.1 - 20191128 - code rewrite
version 1.2 - 20191129 - add advanced options, muting/soloing
version 1.3 - 20191130 - better debug routines, for easier dev of future features
version 1.4 - 20191201 - refining settings system
version 1.5 - 20191203 - code cleanup, finetune whitespace behaviour logic, add settings to Common Controls (or Settings, whatever it is) tab
version 1.6 - 20191204 - fixed one last subtle bug in LinesCount, WSL Patreon first public release
version 1.61- 20200329 - made available free of charge with no strings attached: https://www.patreon.com/posts/wsl-patronage-35399491

--]]

version = "Suck Less Write On v1.61 - 29 Mar 2020"
message = "Thank you for supporting We Suck Less!"
dbg = 0

-------------------------------------------------------------------
-------------------------------------------------------------------
FuRegisterClass("SuckLessWriteOn", CT_Modifier, {
	REGS_Category       = "Fuses",
	REGS_OpIconString   = "WWO",
	REGS_OpDescription  = "Write text one word at a time",
	REGS_Name           = "Suck Less Write On",
	REGID_DataType      = "Text",
	REGID_InputDataType = "Text",
	REGS_Company 		= "Pieter Van Houte",
	REGS_URL 			= "https://www.secondman.com",
	REGS_HelpTopic      = "",
	REG_Fuse_NoEdit     = true,
	REG_Fuse_NoReload   = true,
	REG_Version			= 161,
	})
	
function Create()
	
	InMode = self:AddInput("Mode", "Mode", {
		LINKID_DataType = "FuID",
		INPID_InputControl = "MultiButtonIDControl",
		INPID_DefaultID = "Absolute",
		INP_DoNotifyChanged  = true,
		MBTNC_ForceButtons = true,
		{ MBTNCD_ButtonWidth = 0.5, MBTNC_AddButton = "Absolute", MBTNCID_AddID = "Absolute", MBTNCS_ToolTip = "Parameter values represent actual line, word or character count. A value of 1 means 1 line, word or character.", },
		{ MBTNC_AddButton = "Relative", MBTNCID_AddID = "Relative", MBTNCS_ToolTip = "Parameter values are relative to total line, word or character count. A value of 1 means all available lines, words or characters.", },
		})
	InStyledText = self:AddInput("Styled Text", "StyledText", {
		LINKID_DataType = "Text",
		INPID_InputControl = "TextEditControl",
		LINK_Main = 1,
		TEC_Lines = 16,
		})
	InLabelName = self:AddInput("Element", "element", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		ICD_Width = 0.8,
		})
	InLabelSolo = self:AddInput("Solo", "solo", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		ICD_Width = 0.1,
		})
	InLabelMute = self:AddInput("Mute", "mute", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		ICD_Width = 0.1,
		})
		
	InLines = self:AddInput("Lines", "Lines", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Integer = true,
		INP_MaxScale = 10,
		INP_Default = 0,
		ICD_Width = 0.8,
		})
	InLinesSolo = self:AddInput("", "LinesSolo", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
	InLinesMute = self:AddInput("", "LinesMute", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
		
	InWords = self:AddInput("Words", "Words", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Integer = true,
		INP_MaxScale = 10,
		INP_Default = 0,
		ICD_Width = 0.8,
		})
	InWordsSolo = self:AddInput("", "WordsSolo", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
	InWordsMute = self:AddInput("", "WordsMute", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
		
	InCharacters = self:AddInput("Characters", "Characters", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Integer = true,
		INP_MaxScale = 10,
		INP_Default = 0,
		ICD_Width = 0.8,
		})
	InCharactersSolo = self:AddInput("", "CharactersSolo", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
	InCharactersMute = self:AddInput("", "CharactersMute", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		ICD_Width = 0.1,
		})
		
	InOOO = self:AddInput("Order", "Order", {
		{ MBTNCD_ButtonWidth = 0.16, MBTNC_AddButton = "LWC", MBTNCID_AddID = "LWC", },
		{ MBTNC_AddButton = "LCW", MBTNCID_AddID = "LCW", },
		{ MBTNC_AddButton = "WLC", MBTNCID_AddID = "WLC", },
		{ MBTNC_AddButton = "WCL", MBTNCID_AddID = "WCL", },
		{ MBTNC_AddButton = "CLW", MBTNCID_AddID = "CLW", },
		{ MBTNC_AddButton = "CWL", MBTNCID_AddID = "CWL", },
		INPID_DefaultID = "LWC",
		LINKID_DataType = "FuID",
		INPID_InputControl = "MultiButtonIDControl",
		MBTNC_StretchToFit = true,
		MBTNC_ForceButtons = true,
		})
	InBypass = self:AddInput("Bypass", "Bypass", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	InVersion = self:AddInput(version, "version", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		})
	InMessage = self:AddInput(message, "message", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		})
		
	self:BeginControlNest("Settings", "Settings", true, {ICS_ControlPage = "Common", });
	
	InWhitespace = self:AddInput("Include Trailing Whitespace", "IncludeTrailingWhitespace", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	InCountEmptyLines = self:AddInput("Count Empty Lines", "CountEmptyLines", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	InCountSpaces = self:AddInput("Count Spaces As Characters", "CountSpacesAsCharacters", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	InMuteOverSolo = self:AddInput("Mute Overrides Solo", "MuteOverridesSolo", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	InOutputInverse = self:AddInput("Output Inverse", "OutputInverse", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})
	
	OutText = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Text",
		LINK_Main = 1,
		})
	
	self:EndControlNest()
end

-------------------------------------------------------------------

function SetSource(out, param, time) -- copies StyledText from Text+ into Modifier
  if out == OutText then
    InStyledText:SetSource(param:Copy(), time)
  end
end

-------------------------------------------------------------------

function NotifyChanged(inp, param, time) -- adjusts input attributes when another input changes
	if inp ~= nil and param ~= nil then
		if inp == InMode then
			if param.Value == "Absolute" then
				InLines:SetAttrs({INPID_InputControl = "ScrewControl",})
				InWords:SetAttrs({INPID_InputControl = "ScrewControl",})
				InCharacters:SetAttrs({INPID_InputControl = "ScrewControl",})
				InLines:SetAttrs({INP_Integer = true,})
				InWords:SetAttrs({INP_Integer = true,})
				InCharacters:SetAttrs({INP_Integer = true,})
				
				InLines:SetAttrs({INP_MaxAllowed = 1000000,})
				InWords:SetAttrs({INP_MaxAllowed = 1000000,})
				InCharacters:SetAttrs({INP_MaxAllowed = 1000000,})
				InLines:SetAttrs({INP_MinAllowed = -1000000,})
				InWords:SetAttrs({INP_MinAllowed = -1000000,})
				InCharacters:SetAttrs({INP_MinAllowed = -1000000,})
				
				InLines:SetAttrs({INP_MaxScale = 10,})
				InWords:SetAttrs({INP_MaxScale = 10,})
				InCharacters:SetAttrs({INP_MaxScale = 10,})
				InLines:SetAttrs({INP_MinScale = 0,})
				InWords:SetAttrs({INP_MinScale = 0,})
				InCharacters:SetAttrs({INP_MinScale = 0,})
			elseif param.Value == "Relative" then
				InLines:SetAttrs({INPID_InputControl = "SliderControl",})
				InWords:SetAttrs({INPID_InputControl = "SliderControl",})
				InCharacters:SetAttrs({INPID_InputControl = "SliderControl",})
				InLines:SetAttrs({INP_Integer = false,})
				InWords:SetAttrs({INP_Integer = false,})
				InCharacters:SetAttrs({INP_Integer = false,})
				
				InLines:SetAttrs({INP_MaxAllowed = 1,})
				InWords:SetAttrs({INP_MaxAllowed = 1,})
				InCharacters:SetAttrs({INP_MaxAllowed = 1,})
				InLines:SetAttrs({INP_MinAllowed = -1,})
				InWords:SetAttrs({INP_MinAllowed = -1,})
				InCharacters:SetAttrs({INP_MinAllowed = -1,})
				
				InLines:SetAttrs({INP_MaxScale = 1,})
				InWords:SetAttrs({INP_MaxScale = 1,})
				InCharacters:SetAttrs({INP_MaxScale = 1,})
				InLines:SetAttrs({INP_MinScale = -1,})
				InWords:SetAttrs({INP_MinScale = -1,})
				InCharacters:SetAttrs({INP_MinScale = -1,})
			end
		end
	end
end

function Relative (element, subcount, precount, set)
	if set:sub(7,7) == "1" then subcount = subcount+1 end -- include trailing whitespace in relative mode
	if element >= 0 then
		element = math.floor(subcount * element)
	elseif element < 0 then
		element = math.ceil(precount * element)
	end
	return(element)
end

function Output (element, cutpoint, subcutpoint, set)
	if element > 0 then
		if dbg == 1 then print ("/"..set.."----------------------------") end
		return cutpoint + subcutpoint
	elseif element < 0 then
		if dbg == 1 then print ("/"..set.."----------------------------") end
		return cutpoint - subcutpoint
	else
		if dbg == 1 then print ("/"..set.."----------------------------") end
		return cutpoint
	end
end

function CountLines (intext, mode, lines, pattern, set, cutpoint)
	if dbg == 1 then print (set.."|"..pattern.."|-----------------------------") end
	
	if set:sub(7,7) == "0" then intext = intext:match("(.-)%s*$") end -- strip trailing whitespace
	
	local subtext = intext:sub(cutpoint+1) -- text left after cutpoint
	local pretext = intext:sub(1,cutpoint):reverse() -- text left up until cutpoint, in reverse
	if dbg == 1 then print ("Subtext: |"..subtext.."|") end
	if dbg == 1 then print ("Pretext: |"..pretext.."|") end
	
	local incutpoint, subcutpoint, precutpoint = 0,0,0
	
	if set:sub(7,7) == "0" then
		_,incutpoint = intext:find("^%s*") -- skip leading whitespace
		_,subcutpoint = subtext:find("^%s*") -- skip leading whitespace
		_,precutpoint = pretext:find("^%s*") -- skip leading whitespace
	end
	
	local whitespacepattern = "(.-)%s*$"
	if set:sub(7,7) == "1" and set:sub(8,8) == "1" then whitespacepattern = "(.-)%s?$" end -- remove trailing whitespace or, if counting empty lines, remove the last newline if there is one. 
	
	local _,linecount = intext:match(whitespacepattern):sub(incutpoint+1):gsub(pattern,"")
	local _,subcount = subtext:match(whitespacepattern):sub(subcutpoint+1):gsub(pattern,"")
	local _,precount = pretext:match(whitespacepattern):sub(precutpoint+1):gsub(pattern,"")
	
	linecount = linecount+1
	subcount = subcount+1
	precount = precount+1
	
	if mode == "Relative" then 
		lines = Relative (lines, subcount, precount, set)
		if dbg == 1 then
			print ("Relative Lines: "..lines)
		end
	end
	
	if dbg == 1 then print ("|"..subtext:sub(1,1).."|") end
	if dbg == 1 then print ("|"..subtext:sub(-1,-1).."|") end
	
	local newlinestart, newlineend
	
	if dbg == 1 then print ("linecount:"..linecount) end
	if dbg == 1 then print ("subcount:"..subcount) end
	if dbg == 1 then print ("precount:"..precount) end
	
	-- now look for newline characters
	
	if lines == 0 then
		subcutpoint = 0
	elseif lines > subcount then
		subcutpoint = string.len(subtext)
	elseif lines < subcount - linecount then
		subcutpoint = string.len(pretext)
	elseif lines > 0 and lines <= subcount then
		for i = 1,lines do
			newlinestart,newlineend = subtext:find(pattern, subcutpoint+1)
			subcutpoint = newlineend
		end
		if newlinestart ~= nil then subcutpoint = newlinestart -1 else subcutpoint = string.len(subtext) end
	elseif lines < 0 and lines >= subcount - linecount then
		for i = 1,math.abs(lines) do
			newlineend,newlinestart = pretext:find(pattern, precutpoint+1)
			precutpoint = newlinestart
		end
		if newlinestart ~= nil then subcutpoint = newlinestart else subcutpoint = string.len(pretext) end
	end
	
	if dbg == 1 then print ("subcutpoint: "..subcutpoint) end
	
	cutpoint = Output (lines, cutpoint, subcutpoint, set)
	
	return cutpoint
	
end

function CountWords (intext, mode, words, pattern, set, cutpoint)
	if dbg == 1 then print (set.."|"..pattern.."|-----------------------------") end
	
	if set:sub(7,7) == "0" then intext = intext:match("(.-)%s*$") end -- strip trailing whitespace
	
	local subtext = intext:sub(cutpoint+1) -- text left after cutpoint
	local pretext = intext:sub(1,cutpoint):reverse() -- text left up until cutpoint, in reverse
	if dbg == 1 then print ("Subtext: |"..subtext.."|") end
	if dbg == 1 then print ("Pretext: |"..pretext.."|") end
	
	local incutpoint, subcutpoint, precutpoint = 0,0,0
	
	if set:sub(7,7) == "0" then
		_,incutpoint = intext:find("^%s*") -- skip leading whitespace
		_,subcutpoint = subtext:find("^%s*") -- skip leading whitespace
		_,precutpoint = pretext:find("^%s*") -- skip leading whitespace
	end
	
	local _,wordcount = intext:gsub(pattern,"")
	local _,subcount = subtext:gsub(pattern,"")
	local _,precount = pretext:gsub(pattern,"")
	
	if mode == "Relative" then words = Relative (words, subcount, precount, set) end
	
	if dbg == 1 then print ("|"..subtext:sub(1,1).."|") end
	if dbg == 1 then print ("|"..subtext:sub(-1,-1).."|") end
	
	local wordstart, wordend
	
	if dbg == 1 then print ("wordcount:"..wordcount) end
	if dbg == 1 then print ("subcount:"..subcount) end
	if dbg == 1 then print ("precount:"..precount) end
	
	-- now look for non-space characters
	
	if words == 0 then
		subcutpoint = 0
	elseif words > subcount then
		subcutpoint = string.len(subtext)
	elseif words <= subcount - wordcount then
		subcutpoint = string.len(pretext)
	elseif words > 0 and words <= subcount then
		for i = 1,words do
			wordstart,wordend = subtext:find(pattern, subcutpoint+1)
			subcutpoint = wordend
		end
	elseif words < 0 and words > subcount - wordcount then
		for i = 1,math.abs(words)+1 do
			wordend,wordstart = pretext:find(pattern, precutpoint+1)
			precutpoint = wordstart
		end
		subcutpoint = wordend-1
	end
	
	if dbg == 1 then print ("subcutpoint: "..subcutpoint) end
	
	cutpoint = Output (words, cutpoint, subcutpoint, set)
	
	return cutpoint
	
end

function CountCharacters (intext, mode, characters, pattern, set, cutpoint)
	if dbg == 1 then print (set.."|"..pattern.."|-----------------------------") end
	
	local counttext = intext -- special case for character counts when counting spaces but not including whitespace (see below, a little dirty)
	
	if set:sub(7,7) == "0" then
		intext = intext:match("(.-)%s*$") -- strip trailing whitespace
		counttext = intext
		if set:sub(9,9) == "1" then
			counttext = counttext:match( "^%s*(.+)" ) -- strip leading whitespace for counting
		end
	end
	
	local subtext = intext:sub(cutpoint+1) -- text left after cutpoint
	local pretext = intext:sub(1,cutpoint):reverse() -- text left up until cutpoint, in reverse
	if dbg == 1 then print ("Subtext: |"..subtext.."|") end
	if dbg == 1 then print ("Pretext: |"..pretext.."|") end
	
	local incutpoint, subcutpoint, precutpoint = 0,0,0
	
	if set:sub(7,7) == "0" then
		_,incutpoint = intext:find("^%s*") -- skip leading whitespace
		_,subcutpoint = subtext:find("^%s*") -- skip leading whitespace
		_,precutpoint = pretext:find("^%s*") -- skip leading whitespace
	end
	
	local _,charactercount = counttext:gsub(pattern,"")
	local _,subcount = counttext:sub(cutpoint+1):gsub(pattern,"")
	local _,precount = counttext:sub(1,cutpoint):gsub(pattern,"")
	
	if mode == "Relative" then characters = Relative (characters, subcount, precount, set) end
	
	if dbg == 1 then print ("|"..subtext:sub(1,1).."|") end
	if dbg == 1 then print ("|"..subtext:sub(-1,-1).."|") end
	
	local characterstart, characterend
	
	if dbg == 1 then print ("charactercount:"..charactercount) end
	if dbg == 1 then print ("subcount:"..subcount) end
	if dbg == 1 then print ("precount:"..precount) end
	
	-- now look for non-space characters
	
	if characters == 0 then
		subcutpoint = 0
	elseif characters > subcount then
		subcutpoint = string.len(subtext)
	elseif characters <= subcount - charactercount then
		subcutpoint = string.len(pretext)
	elseif characters > 0 and characters <= subcount then
		for i = 1,characters do
			characterstart,characterend = subtext:find(pattern, subcutpoint+1)
			subcutpoint = characterend
		end
	elseif characters < 0 and characters > subcount - charactercount then
		for i = 1,math.abs(characters)+1 do
			characterend,characterstart = pretext:find(pattern, precutpoint+1)
			precutpoint = characterstart
		end
		subcutpoint = characterend-1
	end
	
	if dbg == 1 then print ("subcutpoint: "..subcutpoint) end
	
	cutpoint = Output (characters, cutpoint, subcutpoint, set)
	
	return cutpoint
	
end

-------------------------------------------------------------------
-------------------------------------------------------------------

function Process(req)

	local mode = InMode:GetValue(req).Value

	local intext = InStyledText:GetValue(req).Value
	
	local lines = InLines:GetValue(req).Value
	local ls = InLinesSolo:GetValue(req).Value
	local lm = InLinesMute:GetValue(req).Value
	local words = InWords:GetValue(req).Value
	local ws = InWordsSolo:GetValue(req).Value
	local wm = InWordsMute:GetValue(req).Value
	local characters = InCharacters:GetValue(req).Value
	local cs = InCharactersSolo:GetValue(req).Value
	local cm = InCharactersMute:GetValue(req).Value
	local ooo = InOOO:GetValue(req).Value
	
	local inwhite = InWhitespace:GetValue(req).Value
	local countlines = InCountEmptyLines:GetValue(req).Value
	local countspaces = InCountSpaces:GetValue(req).Value
	local mos = InMuteOverSolo:GetValue(req).Value
	local outinv = InOutputInverse:GetValue(req).Value
	
	local bypass = InBypass:GetValue(req).Value
	
	local linepattern = "\n+"
	local wordpattern = "%S+"
	local charpattern = "%S"
	
	if countlines == 1 then linepattern = "\n" end
	if countspaces == 1 then charpattern = "." end
	
	local setl, setw, setc = 	"LINES_"..inwhite..countlines..countspaces..mos..outinv,
														"WORDS_"..inwhite..countlines..countspaces..mos..outinv,
														"CHARS_"..inwhite..countlines..countspaces..mos..outinv
	
	lines = lines*(1-lm)*(math.min(1,1-ws+mos*wm))*(math.min(1,1-cs+mos*cm))
	words = words*(1-wm)*(math.min(1,1-ls+mos*lm))*(math.min(1,1-cs+mos*cm))
	characters = characters*(1-cm)*(math.min(1,1-ls+mos*lm))*(math.min(1,1-ws+mos*wm))

	local o = {
		L = {func = CountLines, var = lines, pat = linepattern, set = setl},
		W = {func = CountWords, var = words, pat = wordpattern, set = setw},
		C = {func = CountCharacters, var = characters, pat = charpattern, set = setc}
		}
	
	local cutpoint = 0 -- start before the beginning
	
	cutpoint = 	o[ooo:sub(3,3)].func (intext, mode, o[ooo:sub(3,3)].var, o[ooo:sub(3,3)].pat, o[ooo:sub(3,3)].set,
							o[ooo:sub(2,2)].func (intext, mode, o[ooo:sub(2,2)].var, o[ooo:sub(2,2)].pat, o[ooo:sub(2,2)].set,
							o[ooo:sub(1,1)].func (intext, mode, o[ooo:sub(1,1)].var, o[ooo:sub(1,1)].pat, o[ooo:sub(1,1)].set,
							cutpoint))
							)
	
	if bypass == 0 then
		if outinv == 0 then
			outtext = intext:sub(1, cutpoint)
		elseif outinv == 1 then
			outtext = intext:sub(cutpoint+1)
		end
	else
		outtext = intext
	end
	
	if dbg == 1 then print ("CUTPOINT: "..cutpoint) end
	if dbg == 1 then print ("OUTTEXT:\n|"..outtext.."|") end
	if dbg == 1 then print ("------------------DONE\n\n") end

	OutText:Set(req, Text(outtext))

end
