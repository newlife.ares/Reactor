"""
Remember flow position script
Inspired by original tool by Michael Vorberg
Python implementation with QT UI
This script stores tool's name and bookmark in current comp metadata.

KEY FEATURES:
* Submit bookmark with ENTER button
* ESC to close the UI
* Rename current bookmark with just reassigning different bookmark name to the same selected tool

Alexey Bogomolov mail@abogomolov.com
Requests and issues: https://github.com/movalex/fusion_scripts/issues
Donations are highly appreciated: https://paypal.me/aabogomolov

MIT License: https://mit-license.org/
"""
# legacy python reporting compatibility
from __future__ import print_function

comp = fu.GetCurrentComp()
ui = fusion.UIManager
flow = comp.CurrentFrame.FlowView

# close UI on ESC button
comp.Execute(
    """app:AddConfig("AddBookmark",
{
    Target  {ID = "AddBookmark"},
    Hotkeys {Target = "AddBookmark",
             Defaults = true,
             ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}" }})
"""
)


def get_bm_name(tool):
    tool_ID_key = int(tool.GetAttrs("TOOLI_ID"))
    bm_data = comp.GetData("BM.tool_ID{}".format(tool_ID_key))
    if bm_data:
        if fu.Version == 16.2:
            return bm_data[1]
        return bm_data.get(1)


def set_underlays(ev):
    all_underlays = comp.GetToolList(False, "Underlay")
    if len(all_underlays) > 0:
        for underlay in all_underlays.values():
            underlay_name = underlay.Name
            tool_bm_name = get_bm_name(underlay)
            if tool_bm_name:
                if tool_bm_name and underlay.Name != tool_bm_name:
                    underlay_name = tool_bm_name
            set_bookmark(underlay, underlay_name)
            disp.ExitLoop()
    else:
        print("no Underlays found")


def set_bookmark(tool, underlay_name=None):
    if tool is False:
        tool = get_tool()
    if not tool:
        return
    """grab current checkbox index and save bookmark data"""
    if underlay_name is None:
        bm_text = itm["BookmarkLine"].GetText()
        if bm_text[0].isdigit():
            print("bookmark name starts with digit, now prepending with _")
            bm_text = "_" + bm_text
            print("created bookmark:", bm_text)
    else:
        bm_text = underlay_name
    current_scale = flow.GetScale()
    tool_name = tool.Name
    tool_reg_id = tool.GetAttrs("TOOLI_ID")
    tool_ID_key = "tool_ID{}".format(int(tool_reg_id))
    comp.SetData(
        "BM.{}".format(tool_ID_key),
        [bm_text, tool_name, current_scale, tool_ID_key, tool.ID],
    )
    print("bookmarked {} as {}".format(tool.ID, bm_text))


def get_tool():
    comp = fu.GetCurrentComp()
    """get either active or selected tool
    from multiple selected tools use the first one"""
    active = comp.ActiveTool
    if active:
        return active
    selected_nodes = list(comp.GetToolList(True).values())
    if len(selected_nodes) > 0:
        return selected_nodes[0]
    print("No tools selected. Select any tool and launch the script again")
    return False


def _close_UI(ev):
    disp.ExitLoop()


def _choose_bm_UI(ev):
    tool = get_tool()
    set_bookmark(tool)
    disp.ExitLoop()


def launch_ui(tool):
    add_enabled = True
    if not tool:
        line_text = "select one tool to add Bookmark"
        add_enabled = False
    else:
        bm_name = get_bm_name(tool)
        if bm_name:
            line_text = bm_name
        else:
            line_text = tool.Name
    disp = bmd.UIDispatcher(ui)

    # Main Window
    win = disp.AddWindow(
        {
            "ID": "AddBookmark",
            "TargetID": "AddBookmark",
            "WindowTitle": "add bookmark",
            "Geometry": [400, 600, 300, 75],
        },
        [
            ui.VGroup(
                [
                    ui.LineEdit(
                        {
                            "ID": "BookmarkLine",
                            "Text": line_text,
                            "Weight": 0.5,
                            "Events": {"ReturnPressed": True},
                            "Alignment": {"AlignHCenter": True},
                        }
                    ),
                    ui.HGroup(
                        [
                            ui.HGap(0, 0.25),
                            ui.Button(
                                {
                                    "ID": "AddButton",
                                    "Text": "OK",
                                    "Enabled": add_enabled,
                                }
                            ),
                            ui.Button(
                                {"ID": "AddUnderlays", "Text": "add Underlays as BM"}
                            ),
                            ui.HGap(0, 0.25),
                        ]
                    ),
                ]
            ),
        ],
    )

    itm = win.GetItems()
    if add_enabled:
        itm["Text"] = tool.Name
        itm["BookmarkLine"].SelectAll()
    return win, itm, disp


if __name__ == "__main__":
    tool = get_tool()
    add_win_exists = ui.FindWindow("AddBookmark")

    if add_win_exists:
        add_win_exists.Raise()
        add_win_exists.ActivateWindow()
        if tool:
            win_itm = add_win_exists.GetItems()
            win_itm["BookmarkLine"].Text = tool.Name
            win_itm["BookmarkLine"].SelectAll()
            win_itm["AddButton"].Enabled = True
    else:
        win, itm, disp = launch_ui(tool)
        win.On.AddButton.Clicked = _choose_bm_UI
        win.On.BookmarkLine.ReturnPressed = _choose_bm_UI
        win.On.AddUnderlays.Clicked = set_underlays
        win.On.AddBookmark.Close = _close_UI
        win.Show()
        disp.RunLoop()
        win.Hide()
