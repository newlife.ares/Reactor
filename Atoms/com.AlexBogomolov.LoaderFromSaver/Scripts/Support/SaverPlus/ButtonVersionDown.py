import re


def version_down(tool):
    comp.Lock()
    comp.StartUndo("Version Down")
    output_old = tool.Clip[1]
    if output_old == "":
        print("No filename specified in saver")
        return
    pattern = re.compile(r"([Vv])(\d{2,})")

    try:
        v_letter, number = re.search(pattern, output_old).groups()
    except AttributeError:
        print("No version pattern found.")
        return
    
    start_count = int(number)
    if start_count <= 1:
        return
    version_new_number = str(start_count - 1).zfill(len(number))
    new_path = re.sub(pattern, "{}{}".format(v_letter, version_new_number), output_old)
    tool.Clip[1] = new_path
    print("[{}] new output: [{}]".format(tool.Name, new_path))
    comp.EndUndo()
    comp.Unlock()


if __name__ == "__main__":
    comp = fu.GetCurrentComp()
    tool = comp.ActiveTool
    if tool and tool.ID == "Saver":
       version_down(tool)
