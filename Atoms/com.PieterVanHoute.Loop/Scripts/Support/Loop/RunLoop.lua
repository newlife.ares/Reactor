--[[

Suck Less Loop Development version - 02 Jan 2021

-------------------------------------------------------------------
Copyright (c) 2021,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------------------------------------

--]]

require "SuckLessDialogs"

print("Loop start...")

-- add routine for checking if script is saved

-- fix for Fu16.2 on Mac (thanks Alexey Bogomolov)
if fu.Version == 16.2 and FuPLATFORM_MAC == true then
    jit.off()
end

-- get our Loop Out
local comp = fusion.CurrentComp
local now = comp.CurrentTime -- for later :)
local loopend = comp:GetToolList(true)[1] -- add check for just one tool selected
--print (loopend:GetAttrs().TOOLS_Name)

-- search function to look for a certain node inside of a group, by name

function FindInGroup (node, group, list)
	for i, tool in ipairs(list) do
		local a = tool:GetAttrs()
		if a.TOOLH_GroupParent ~= nil then
			if string.find( a.TOOLS_Name, node ) and a.TOOLH_GroupParent:GetAttrs().TOOLS_Name == group:GetAttrs().TOOLS_Name then
				--print("LOOP input found")
				--print(a.TOOLS_Name)
				return tool
			end
		end
	end
end

function PurgeLoader(loader)
	-- workaround to prevent having to purge cache
	loader.Loop = 1
	loader.Loop = 0
end
	
	

-- get the render range that was set before running the script, because we will want to reset this after
local startframe = comp:GetAttrs().COMPN_RenderStart
local endframe = comp:GetAttrs().COMPN_RenderEnd

if (loopend:GetAttrs().TOOLS_RegID == "GroupOperator" or loopend:GetAttrs().TOOLS_RegID == "MacroOperator") then -- improve this check (see above define loopend)

	-- then get our Loop Start
	local loopstart = loopend.LoopStart:GetConnectedOutput():GetTool():GetAttrs().TOOLH_GroupParent 
	print (loopstart:GetAttrs().TOOLS_Name)

	local path = loopend.CacheFolder[fu.TIME_UNDEFINED]
	if path == "" then
		SuckLessMsgDialog("Suck More!", "No Cache Folder is defined in Loop Out node.", "OK")
		print ("Loop broken. No loop for you.")
		return
	end
	
	local name = loopend.SolveName[fu.TIME_UNDEFINED]
	local rstart = loopend.StartFrame[fu.TIME_UNDEFINED] 
	local rend = rstart + loopend.Frames[fu.TIME_UNDEFINED] -1 
	local iterations = loopend.Iterations[fu.TIME_UNDEFINED]
--	local finit = loopend.FastInit[fu.TIME_UNDEFINED]
	local initrstep = math.max(rend-rstart,1) -- render step for placeholder frames
	
	-- define which inputs and outputs we're going to work with in the currently selected Macro
	-- print (tool:GetAttrs().TOOLS_RegID)
	local loaderlist = comp:GetToolList(false, "Loader")
	local saverlist = comp:GetToolList(false, "Saver")
	local switchlist = comp:GetToolList(false, "Dissolve")
	local timelist = comp:GetToolList(false, "TimeStretcher")
	
	-- look for our inputs, outputs and switches inside the selected Loop nodes
	
	___SUCKLESS_LOOP_IN___ = FindInGroup("___SUCKLESS_LOOP_IN___",loopstart,loaderlist)
	___SUCKLESS_LOOP_RESULT___ = FindInGroup("___SUCKLESS_LOOP_RESULT___",loopend,loaderlist)
	___SUCKLESS_LOOP_OUT___ = FindInGroup("___SUCKLESS_LOOP_OUT___",loopend,saverlist)
	___SUCKLESS_SWITCH_IN___ = FindInGroup("___LOOP_IN_CONTROLS___",loopstart,switchlist)
	___SUCKLESS_SWITCH_OUT___ = FindInGroup("___LOOP_OUT_CONTROLS___",loopend,switchlist)
	___SUCKLESS_SWITCH_INIT___ = FindInGroup("___LOOP_INIT___",loopend,switchlist)
	___SUCKLESS_SWITCH_FF___ = FindInGroup("___LOOP_FF___",loopend,switchlist)
	___SUCKLESS_TIME_IN___ = FindInGroup("___TIME_IN___",loopstart,timelist)
		
	print (___SUCKLESS_TIME_IN___)
	
	-- now that we have found the Saver, let's populate it with the Cache Files path set in the Solver, and render it, but only it
	
	rgbapath = path..name..string.format("%06d",rstart)..".exr"
	print(rgbapath)		-- also here better checks are needed
	
	comp:Lock()
	
	-- get and set some preferences
	
	framesatonce = comp:GetPrefs("Comp.Memory.FramesAtOnce")
	comp:SetPrefs("Comp.Memory.FramesAtOnce", 1)
	
	-- make sure Loop can render by setting Render Abort to false in the comp CustomData
	comp:SetData ("Suck Less Loop Render Abort", false)
	
	-- get the current state of all Savers and put them in a table, then set all of them to passthrough
	
	local saversettings = {}
	for i, tool in ipairs(saverlist) do
		local a = tool:GetAttrs()
			saversettings[tool] = a.TOOLB_PassThrough
			tool:SetAttrs({TOOLB_PassThrough = true})
	end
	
	-- RESET SWITCHES TO STARTING STATES
	
	___SUCKLESS_SWITCH_IN___.Mix = 0
	___SUCKLESS_SWITCH_INIT___.Mix = 0
	___SUCKLESS_SWITCH_FF___.Mix = 0
	___SUCKLESS_SWITCH_OUT___.Mix = 0
	
	-- INITIALISE THE LOOP
		
	___SUCKLESS_SWITCH_INIT___.Mix = 1
	___SUCKLESS_LOOP_OUT___:SetAttrs({TOOLB_PassThrough = false})
	___SUCKLESS_LOOP_OUT___.Clip = rgbapath
	
	-- render, then passthrough our init saver again
	
	print ("Rendering placeholder frames for output...")
	print (rstart.."-"..rend.."-"..initrstep)
	err = comp:Render(true, rstart, rend, initrstep)
	
	-- we now have empty frames, hurray, now let's set the ___SUCKLESS_LOOP_IN___

	___SUCKLESS_LOOP_IN___.Clip[rstart] = rgbapath
	___SUCKLESS_LOOP_IN___.GlobalIn[rstart] = rstart
	___SUCKLESS_LOOP_IN___.ClipTimeStart[rstart] = 0
	
	-- turn off INIT switch

	___SUCKLESS_SWITCH_INIT___.Mix = 0
	
	-- RENDER THE REST OF THE LOOP
	
	--SuckLessMsgDialog("Suck Less Loop", "Loop is rendering, check the Console for details...", "Abort Render")

	for frame = rstart, rend, 1 do
	
		--print (comp:GetData ("Suck Less Loop Render Abort"))
		PurgeLoader(___SUCKLESS_LOOP_IN___)
		
		if frame == rstart then
			___SUCKLESS_SWITCH_IN___.Mix = 0
		end
	
		if comp:GetData ("Suck Less Loop Render Abort") == false then
			print ("\n\nRendering frame "..frame.." of "..rend.."\n\n")
			err = composition:Render(true, frame, frame, 1)
			if frame == rstart then
			___SUCKLESS_SWITCH_IN___.Mix = 1
			end
			if iterations > 1 then
				___SUCKLESS_TIME_IN___:SetAttrs({TOOLB_PassThrough = true})
				for iter = 1, iterations-1, 1 do
					PurgeLoader(___SUCKLESS_LOOP_IN___)
					err = composition:Render(true, frame, frame, 1)
				end
				___SUCKLESS_TIME_IN___:SetAttrs({TOOLB_PassThrough = false})
			end
		end
	end
	
	-- finally, set switches and output to read the rendered frames from disk
	
	___SUCKLESS_LOOP_RESULT___.Clip[rstart] = rgbapath
	___SUCKLESS_LOOP_RESULT___.GlobalIn = rstart
	___SUCKLESS_LOOP_RESULT___.ClipTimeStart[rstart] = 0
	___SUCKLESS_SWITCH_OUT___.Mix = 1

	-- reset all Savers, switch output and set preferences to initial state
		
	for saver, setting in pairs(saversettings) do
		saver:SetAttrs({TOOLB_PassThrough = setting})
	end
	
	comp:SetAttrs({COMPN_RenderStart = startframe})
	comp:SetAttrs({COMPN_RenderEnd = endframe})
	
	comp:SetPrefs("Comp.Memory.FramesAtOnce", framesatonce)
	comp:SetData ("Suck Less Loop Render Abort", false)

	comp:Unlock()

	print ("Suck Less Loop is all done for now...")
	
else
	SuckLessMsgDialog("Suck More!", "Please make sure one Loop Out node is selected before running the Loop.", "OK")
	print ("Loop exited. No loop for you.")
	return
end



